import React, { Component, useState } from "react";
import { View, Button } from "react-native";
import DateTimePickerModal from "react-native-modal-datetime-picker";

const DateTimePicker = ({ type, buttonTitle, dateKey, setSchedule }) => {
  const [DatePickerVisible, setDatePickerVisibility] = useState(false);

  const showDatePicker = () => {
    setDatePickerVisibility(true);
  };

  const hideDatePicker = () => {
    setDatePickerVisibility(false);
  };

  const handleConfirm = (date) => {
    if (type === "time") {
      // Extrair apenas hora e minuto
      const hour = date.getHours();
      const minute = date.getMinutes();

      // Formatar hora e minuto como desejado
      const formattedTime = `${hour}:${minute}`;

      // Atualizar o estado com a hora e o minuto formatados
      setSchedule((prevState) => ({
        ...prevState,
        [dateKey]: formattedTime,
      }));
    }else{
        const formattedDate = date.toISOString().split('T')

    setSchedule((prevState) => ({
      ...prevState,
      [dateKey]: formattedDate,
    }));
    }
    hideDatePicker();
  };

  return (
    <View>
      <Button title={buttonTitle} onPress={showDatePicker} color="#888" />
      <DateTimePickerModal
        isVisible={DatePickerVisible}
        mode={type}
        locale="pt_BR"
        onConfirm={handleConfirm}
        onCancel={hideDatePicker}
        // Ajuste o estilo do modal
        textColor="black" // Cor do texto
      />
    </View>
  );
};
export default DateTimePicker;
